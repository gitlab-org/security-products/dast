import os
from pathlib import Path
from tempfile import NamedTemporaryFile
from unittest import TestCase
from unittest.mock import patch

from src.configuration import Configuration
from test.unit.mock_config import ToConfig


class TestConfiguration(TestCase):

    def test_has_no_pkcs_cert_filename_if_there_is_no_cert(self):
        config = Configuration(ToConfig(pkcs12_cert=None, dast_major_version=2))
        self.assertEqual('', config.pkcs12_cert_filename)

    def test_writes_cert_contents_to_file(self):
        config = Configuration(ToConfig(pkcs12_cert=b'cert.content', dast_major_version=2))

        try:
            cert_content = Path(config.pkcs12_cert_filename).read_text()
            self.assertEqual('cert.content', cert_content)
        finally:
            if os.path.exists(config.pkcs12_cert_filename):
                os.remove(config.pkcs12_cert_filename)

    def test_only_writes_cert_file_once(self):
        with patch('src.configuration.mkstemp') as mock_temp_file:
            mock_temp_file.return_value = NamedTemporaryFile().name, '/path/to/file'

            config = Configuration(ToConfig(pkcs12_cert=b'cert.content', dast_major_version=2))
            self.assertEqual('/path/to/file', config.pkcs12_cert_filename)
            self.assertEqual('/path/to/file', config.pkcs12_cert_filename)

        mock_temp_file.assert_called_once()
