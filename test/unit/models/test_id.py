import json
from unittest import TestCase

from src.models import AlertID, ID, RuleID
from src.services import PythonObjectEncoder


class TestID(TestCase):

    def test_can_be_cast_as_int(self):
        id_obj = ID('5')
        self.assertEqual(int(id_obj), 5)

    def test_throws_error_when_cast_to_int_if_invalid(self):
        id_obj = ID('error')

        with self.assertRaises(ValueError) as error:
            int(id_obj)

        self.assertEqual(str(error.exception), 'ID must be an integer')

    def test_can_be_cast_as_str(self):
        id_obj = ID('5')

        self.assertEqual(str(id_obj), '5')

    def test_casts_to_empty_string_if_invalid(self):
        id_obj = ID('error')

        self.assertEqual(str(id_obj), '')

    def test_is_json_serializable(self):
        id_obj = ID('6')

        self.assertEqual(json.dumps(id_obj, cls=PythonObjectEncoder), '"6"')

    def test_is_equal_to_ids_with_same_value_and_type(self):
        alert_id_1 = AlertID('6')
        alert_id_2 = AlertID('6')
        alert_id_3 = AlertID('7')
        rule_id = RuleID('6')

        self.assertEqual(alert_id_1, alert_id_2)
        self.assertNotEqual(alert_id_1, alert_id_3)
        self.assertNotEqual(alert_id_1, rule_id)

    def test_valid_returns_true_if_id_is_digit(self):
        id_obj = ID('5')

        self.assertTrue(id_obj.valid())

    def test_valid_returns_false_if_id_is_not_present(self):
        id_obj = ID('')

        self.assertFalse(id_obj.valid())

    def test_valid_returns_false_if_id_is_not_digit(self):
        id_obj = ID('error')

        self.assertFalse(id_obj.valid())
