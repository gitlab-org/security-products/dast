import logging
from typing import List, Optional
from urllib.parse import urljoin

from src.models import Target
from src.utilities import is_url


class APITargetSelector:

    def __init__(self, urls: List[str], host_override: Optional[str] = None):
        self._urls = urls
        self._host_override = host_override

    def select(self) -> Target:
        target = self._urls[0]

        logging.debug(f'Setting target to URL from API specification: {target}')

        if self._host_override:
            target = self._apply_host_override(target)

            logging.debug(f'Setting target to new URL with host override: {target}')

        return Target(target)

    def _apply_host_override(self, target: str) -> str:
        host_override = self._host_override

        if not is_url(host_override):
            host_override = f'//{host_override}'

        return urljoin(target, host_override)
