from datetime import datetime

from src.models.http import HttpMessage, HttpRequest, HttpResponse
from .http_request import http_request as http_request_builder
from .http_response import http_response as http_response_builder


def http_message(message_id: str = '1',
                 request: HttpRequest = None,
                 response: HttpResponse = None,
                 time_sent: datetime = datetime(2020, 10, 30, 23, 58, 59, 123000)) -> HttpMessage:
    http_request = request if request else http_request_builder()
    http_response = response if response else http_response_builder()

    return HttpMessage(message_id, http_request, http_response, time_sent)
