import logging
import os
from typing import List, Optional

from src.models.errors import InvalidAPISpecificationError, NoUrlsError
from src.utilities import is_url
from src.zap_gateway import Settings, ZAProxy


class APISpecification:

    def __init__(self, zap: ZAProxy, api_specification: str, host_override: Optional[str]):
        self._zap = zap
        self._spec = api_specification
        self._host_override = host_override

    def load(self) -> List[str]:
        if not self._spec_is_valid():
            raise InvalidAPISpecificationError('Target must be either a valid URL or a local file, '
                                               f'file does not exist: {self._spec_file_path}')

        import_result = self._import_spec()

        urls = self._zap.urls()

        logging.debug(f'Import warnings: {str(import_result)}')

        if len(urls) == 0:
            raise NoUrlsError('Failed to import any URLs defined in API specification')

        logging.info(f'Number of imported URLs: {str(len(urls))}')

        return urls

    def _import_spec(self) -> List[str]:
        if is_url(self._spec):
            logging.debug(f'Import OpenAPI URL {self._spec}')

            return self._zap.import_api_spec_from_url(self._spec, self._host_override)

        logging.debug(f'Import OpenAPI File {self._spec}')

        if self._host_override:
            logging.warning(
                'Host override is not compatible with loading API specification '
                'from a file and will be ignored',
            )

        return self._zap.import_api_spec_from_file(self._spec_file_path)

    def _spec_is_valid(self) -> bool:
        return (is_url(self._spec)
                or (os.path.exists(self._spec_file_path) and self._spec_file_path.startswith(Settings.WRK_DIR)))

    @property
    def _spec_file_path(self) -> str:
        return os.path.realpath(f'{Settings.WRK_DIR}{self._spec}')
