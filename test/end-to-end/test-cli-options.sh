#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null
  docker run --rm --name nginx -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro --network test -d nginx:1.22.0 >/dev/null
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_jvm_args_can_be_set() {
  skip_if_fips "Testing ZAP startup"

  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    -e DAST_ZAP_CLI_OPTIONS="-Xmx4072m" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://nginx >output/test_jvm_args_can_be_set.log 2>&1

  grep -q "Using JVM args: -Xmx4072m" output/test_jvm_args_can_be_set.log
  assert_equals "0" "$?" "JVM args not found in log file"
}

test_zap_log_config_can_be_customized() {
  skip_if_fips "No ZAP log without ZAP"

CUSTOM_CONFIGURATION="logger.httpsender.name=org.parosproxy.paros.network.HttpSender;logger.httpsender.level=debug;logger.sitemap.name=org.parosproxy.paros.model.SiteMap;logger.sitemap.level=debug;"

  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --network test \
    -e DAST_ZAP_LOG_CONFIGURATION="$CUSTOM_CONFIGURATION" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://nginx >output/test_zap_log_config_can_be_customized.log 2>&1


  grep -q "DEBUG org.parosproxy.paros.network.HttpSender" output/test_zap_log_config_can_be_customized.log | \
  grep -q "DEBUG org.parosproxy.paros.model.SiteMap" output/test_zap_log_config_can_be_customized.log
  assert_equals "0" "$?" "HttpSender DEBUG messages not found in log file"
}

test_help_documentation() {
  # Intentionally not using docker_run_dast
  docker run --rm \
    "${BUILT_IMAGE}" /analyze --help \
     >output/test_help_documentation.log 2>&1
  assert_equals "0" "$?" "Help documentation was not outputed, test failed."

  grep -q "usage: analyze.py \[-h\] \[-t DAST_WEBSITE\]" output/test_help_documentation.log &&
  grep -q "auth-exclude-urls DAST_EXCLUDE_URLS" output/test_help_documentation.log
  assert_equals "0" "$?" "Error message not present"
}

test_invalid_configuration() {
  # Intentionally not using docker_run_dast
  docker run --rm \
    "${BUILT_IMAGE}" /analyze \
     >output/test_invalid_configuration.log 2>&1
  assert_equals "1" "$?" "Expected DAST to fail, but it did not."

  grep -q "InvalidConfigurationError: Either DAST_WEBSITE, DAST_API_OPENAPI (deprecated), or DAST_API_SPECIFICATION must be set." output/test_invalid_configuration.log
  assert_equals "0" "$?" "Error message not present"
}
