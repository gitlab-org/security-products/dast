# DAST coding principles

Learn the processes and technical information needed for contributing to GitLab
DAST. This content is intended for members of the GitLab Team as well as
community contributors.

## Dependency management

- Prefer to have many instances of the same service class over a single class
    being instantiated and passed through to other services
- Prefer to instantiate services in the method where they are used over
    instantiating them in the constructor
- Prefer to instantiate services that contain state as low-down as possible. For
    example, if AlertRepository and MessageRepository depend on the stateful
    Database, then Database should be instantiated as close to the
    repositories as possible.
- Using `dependencies.py` for dependency management is deprecated. Please inline
  dependencies from the file where possible.

## Naming

- Prefer upcased acronyms in class names. For example, a DAST error handler
    should be named `DASTErrorHandler` and not `DastErrorHandler`. Occasionally,
    a class name may include 2 or more acronyms in a row (Ex: `CweID`). In these
    cases, capitalize acronyms where possible while preserving readability.
- The name of the class should correspond to the name of the Python file.
    For example, the class `TestActiveScan` should be found in a file called `test_active_scan.py`.

## Factories

Factories should be used when creating instances of model objects in tests.

### Benefits

- Changing a model constructor results in a change to one factory instead of many tests.
- Tests use models that are conceptually valid.
  - E.g. `Alert({'pluginId': '1'})` works for some tests, but is not a valid `Alert` because it's missing required fields.
- Tests can instantiate a model with just the values they care about.
- Tests can use real model objects instead of mocks.

### Guidelines

- Factories should live in the `test/unit/factories` folder/namespace.
- Prefer to create the factory in the same MR that creates the model object.
- Prefix each factory with `f_` at the start of the name.
  - The `f_` stands for factory and is deliberately short as a consequence of how often it will be used.
  - The prefix removes a name conflict by enabling code in tests such as `alert = f_alert()`.
- Prefer to import the factory directly, instead of the package. For example, prefer `f_alert()` over `factories.model.f_alert()`.
- For consistency, collection classes should also use factories. This also enables future extensions such as generators.
  - Passing `None` to a model collection factory should return an empty model collection e.g. `f_alerts(None)` should return `Alerts([])`.

### ZAP API factory methods

The Python ZAP client library has methods such as `zap.core.version` and `zap.pscan.scanners`.
These will typically be called from `ZAProxy`, which acts as a wrapper around the ZAP client library.

Where possible, please use factories to capture responses from the ZAP API:

- ZAP API factories help engineers understand ZAP responses without having to debug the application.
- The factory for the response should live in a folder that matches the call. For example, the factory for the
`zap.pscan.scanners` response should live in `test/unit/factories/zap_api/pscan/scanners`.
