## Developer Certificate of Origin + License

By contributing to GitLab Inc., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab Inc.
Except for the license granted herein to GitLab Inc. and recipients of software
distributed by GitLab Inc., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Issue tracker

To get support for your particular problem please use the
[getting help channels](https://about.gitlab.com/getting-help/).

The [GitLab issue tracker on GitLab.com][tracker] is the right place for bugs and feature proposals about Security Products and DAST. 
Please use ~"devops:secure" and "~group::dynamic analysis" labels when opening a new issue regarding DAST to ensure it is quickly reviewed by the right people. Please refer to our [review response SLO][review] to understand when you should receive a response.

**[Search the issue tracker][tracker]** for similar entries before
submitting your own, there's a good chance somebody else had the same issue or
feature proposal. Show your support with an award emoji and/or join the
discussion. 

[tracker]: https://gitlab.com/gitlab-org/gitlab/issues
[review]: https://docs.gitlab.com/ee/development/code_review.html#review-response-slo

