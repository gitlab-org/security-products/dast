#!/bin/bash

set -e  # fail if non-zero exit code

if [[ -n "$DAST_PKCS12_CERTIFICATE_BASE64" ]]; then
    echo "Configuring proxy to use Mutual TLS"

    CERTS_DIR="/browserker/certs"
    PKCS12_CERT="$CERTS_DIR/pkcs12cert.p12"

    # Decode base64 cert
    echo "$DAST_PKCS12_CERTIFICATE_BASE64" | base64 -d >"$PKCS12_CERT"

    # Use a password if provided
    if [[ -n "$DAST_PKCS12_PASSWORD" ]]; then
        pkcs12pass_arg="-passin env:DAST_PKCS12_PASSWORD"
    else
        pkcs12pass_arg=""
    fi

    # Extract the client certificates, CA certificates, and the private key.
    # shellcheck disable=SC2086
    openssl pkcs12 -nokeys -clcerts -in "$PKCS12_CERT" -out "$CERTS_DIR/pkcs12cert-certificate.crt" $pkcs12pass_arg
    # shellcheck disable=SC2086
    openssl pkcs12 -nokeys -cacerts -in "$PKCS12_CERT" -out "$CERTS_DIR/pkcs12cert-ca-cert.ca" $pkcs12pass_arg
    # shellcheck disable=SC2086
    openssl pkcs12 -nocerts -in "$PKCS12_CERT" -out "$CERTS_DIR/pkcs12cert-private.key" $pkcs12pass_arg -passout pass:tmp_pwd

    # Replace the private key with a key that has no passphrase. Otherwise, Squid will ask for one when it starts.
    openssl rsa -in "$CERTS_DIR/pkcs12cert-private.key" --passin pass:tmp_pwd -out "$CERTS_DIR/pkcs12cert-private-no-pass.key"

    # Create the PEM file for Squid to use
    cat "$CERTS_DIR/pkcs12cert-certificate.crt"  \
        "$CERTS_DIR/pkcs12cert-ca-cert.ca" \
        "$CERTS_DIR/pkcs12cert-private-no-pass.key" > "$CERTS_DIR/mutual-tls-client-auth.pem"

    # Add Squid configuration entry to use cert if asked
    echo "tls_outgoing_options cert=$CERTS_DIR/mutual-tls-client-auth.pem" >> /var/squid/etc/squid.conf
fi
