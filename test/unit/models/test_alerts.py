from unittest import TestCase
from unittest.mock import MagicMock

from src.models import Alerts, RuleID
from test.unit.factories.models import f_alert


class TestAlerts(TestCase):

    def setUp(self):
        self.message = {}

    def test_responds_to_len(self):
        alerts = Alerts(['my-fake-alert'])

        self.assertEqual(len(alerts), 1)

    def test_is_subscriptable(self):
        alert = MagicMock(name='alert')
        alerts = Alerts([alert])

        self.assertEqual(alerts[0], alert)

    def test_returns_alerts_by_rule_id(self):
        alert_1 = f_alert(plugin_id='1')
        alert_2 = f_alert(plugin_id='2')
        alerts = Alerts([alert_1, alert_2])

        results = alerts.find_by_rule_id(RuleID('1'))
        self.assertEqual(1, len(results))
        self.assertEqual(alert_1, results[0])

    def test_returns_no_alerts_when_rule_id_not_found(self):
        alerts = Alerts([]).find_by_rule_id(RuleID('666'))
        self.assertEqual(0, len(alerts))

    def test_returns_a_sample_of_alerts(self):
        alert_1 = f_alert(plugin_id='1')
        alert_2 = f_alert(plugin_id='2')
        alerts = Alerts([alert_1, alert_2])

        results = alerts.sample(1)
        self.assertEqual(1, len(results))
        self.assertEqual(alert_1, results[0])

    def test_returns_no_samples_if_there_are_no_alerts(self):
        self.assertEqual(0, len(Alerts([]).sample(1)))

    def test_returns_as_many_samples_as_possible(self):
        results = Alerts([f_alert()]).sample(100)
        self.assertEqual(1, len(results))

    def test_sort_by_returns_lambda_with_sort_keys(self):
        alert = f_alert(url='https://test.site', param='loltest', method='DELETE', attack='hack')
        alerts = Alerts([alert])

        sort_by = alerts.sort_by(['url', 'method', 'param', 'attack'])

        self.assertEqual(sort_by(alert), 'https://test.site:DELETE:loltest:hack')

    def test_noisy_alerts_are_aggregated(self):
        noisy_alert = f_alert(plugin_id='10015', name='Noisy Alert')
        alert_1 = f_alert(plugin_id='1', name='Alert 1')
        noisy_alert_1 = f_alert(plugin_id='10015', name='Noisy Alert 1')
        alert_2 = f_alert(plugin_id='2', name='Alert 2')

        alerts = Alerts([noisy_alert, alert_1, noisy_alert_1, alert_2])
        aggregated_alerts = alerts.sort()
        aggregated_alerts = aggregated_alerts.aggregate()

        self.assertEqual(3, len(aggregated_alerts))
        self.assertEqual(noisy_alert.name, aggregated_alerts[2].aggregated_alerts()[0].name)
        self.assertEqual(noisy_alert_1.name, aggregated_alerts[2].aggregated_alerts()[1].name)

    def test_sort_alerts(self):
        alert_1 = f_alert(name='B', method='POST')
        alert_2 = f_alert(name='B', method='GET')
        alert_3 = f_alert(name='A', method='POST')
        alert_4 = f_alert(name='A', method='GET')

        alerts = Alerts([alert_1, alert_2, alert_3, alert_4])
        sorted_alerts = alerts.sort()
        self.assertAlertsAreEqual(alert_3, sorted_alerts[0])
        self.assertAlertsAreEqual(alert_4, sorted_alerts[1])
        self.assertAlertsAreEqual(alert_1, sorted_alerts[2])
        self.assertAlertsAreEqual(alert_2, sorted_alerts[3])

    def assertAlertsAreEqual(self, alert_1, alert_2):
        self.assertEqual(alert_1.name, alert_2.name)
        self.assertEqual(str(alert_1.rule_id), str(alert_2.rule_id))
