#!/usr/bin/env bash
# Testing framework: https://github.com/pgrange/bash_unit

# DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

test_proxy_started() {

  local container_id
  local runtime
  local endtime
  local rc

  # Start the container up and keep it running for a while
  container_id=$(docker run --rm --detach "$BUILT_IMAGE" sh -c "sleep 1000")

  # Check for squid process
  # Initialization time varies, so we wait for up to N minutes

  runtime="3 minutes"
  endtime=$(date -ud "$runtime" +%s)
  assert_ret=0
  while [[ $(date -u +%s) -le $endtime ]]
  do
    # Check for squid proces
    docker exec "$container_id" ps ax | grep "/var/squid/sbin/squid" >/dev/null 2>&1
    assert_ret=$?

    if [[ $assert_ret -eq 0 ]]; then
      break
    fi

    sleep 0.2
  done
  if [[ $assert_ret -ne 0 ]]; then
    docker stop "$container_id"
    assert_equals "0" "$assert_ret" "Expected to find /var/squid/sbin/squid in the process list"
  fi

  # Check for security_file_certgen process
  # Initialization time varies, so we wait for up to N minutes

  runtime="3 minutes"
  endtime=$(date -ud "$runtime" +%s)
  assert_ret=0
  while [[ $(date -u +%s) -le $endtime ]]
  do
    # Check for security_file_certgen process
    docker exec "$container_id" ps ax | grep "security_file_certgen" >/dev/null 2>&1
    assert_ret=$?

    if [[ $assert_ret -eq 0 ]]; then
      break
    fi

    sleep 0.2
  done
  if [[ $assert_ret -ne 0 ]]; then
    docker stop "$container_id"
    assert_equals "0" "$assert_ret" "Expected to find security_file_certgen in the process list"
  fi

  docker exec "$container_id" curl --silent --proxy http://localhost:3128 https://gitlab.com  >> output/test_fips_proxy_started-cmds.log 2>&1
  rc=$?
  docker stop "$container_id"
  assert_equals "0" "$rc" "Expected to fetch https://gitlab.com via squid without errors"
}