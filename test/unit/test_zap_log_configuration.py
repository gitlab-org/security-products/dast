import unittest
from io import StringIO

from src.zap_log_configuration import ZAPLogConfiguration
from test.unit.mock_config import ToConfig


class TestZAPLogConfiguration(unittest.TestCase):

    def setUp(self):
        self.stringio = StringIO()
        self.external_config = ToConfig(zap_log_configuration=None)
        self.extra_properties = 'HttpSender=DEBUG;CRAWLER=WARN'
        self.base_properties = (
            '# Properties below added programmtically by DAST\n'
            'name = ZAP Home Config\n'
            'rootLogger.level = info\n'
            'rootLogger.appenderRef.stdout.ref = stdout\n'
            'rootLogger.appenderRef.rolling.ref = RollingFile\n'
            'appender.console.type = Console\n'
            'appender.console.name = stdout\n'
            'appender.console.layout.type = PatternLayout\n'
            'appender.console.layout.pattern = %r [%t] %-5level %logger{36} - %msg%n\n'
            'property.filename = ${sys:zap.user.log}/zap.log\n'
            'appender.rolling.type = RollingFile\n'
            'appender.rolling.name = RollingFile\n'
            'appender.rolling.fileName = ${filename}\n'
            'appender.rolling.filePattern = ${filename}.%i\n'
            'appender.rolling.immediateFlush = true\n'
            'appender.rolling.layout.type = PatternLayout\n'
            'appender.rolling.layout.pattern = %d [%-5t] %-5p %c{1} - %m%n\n'
            'appender.rolling.policies.type = Policies\n'
            'appender.rolling.policies.size.type = SizeBasedTriggeringPolicy\n'
            'appender.rolling.policies.size.size=1024MB\n'
            'appender.rolling.strategy.type = DefaultRolloverStrategy\n'
            'appender.rolling.strategy.max = 3\n'
            'logger.commonshttpclient.name = org.apache.commons.httpclient\n'
            'logger.commonshttpclient.level = error\n'
            'logger.jericho.name = net.htmlparser.jericho\n'
            'logger.jericho.level = off\n'
            'logger.crawljaxCrawler.name = com.crawljax.core.Crawler\n'
            'logger.crawljaxCrawler.level = warn\n'
            'logger.crawljaxStateMachine.name = com.crawljax.core.state.StateMachine\n'
            'logger.crawljaxStateMachine.level = warn\n'
            'logger.crawljaxUnfired.name = com.crawljax.core.UnfiredCandidateActions\n'
            'logger.crawljaxUnfired.level = warn\n'
            'logger.hsqldb.name = hsqldb.db.HSQLDB379AF3DEBD.ENGINE\n'
            'logger.hsqldb.level = warn'
        )

        self.immutable_properties = (
            'logger.httpSessionsSite.name = org.zaproxy.zap.extension.httpsessions.HttpSessionsSite\n'
            'logger.httpSessionsSite.level = off\n'
            'logger.abstractParam.name = org.parosproxy.paros.common.AbstractParam\n'
            'logger.abstractParam.level = off\n'
            'logger.extensionReplacer.name = org.zaproxy.zap.extension.replacer.ExtensionReplacer\n'
            'logger.extensionReplacer.level = info\n'
            'logger.apacheHeaders.name = org.apache.hc.client5.http.headers\n'
            'logger.apacheHeaders.level = info\n'
            'logger.apacheWire.name = org.apache.hc.client5.http.wire\n'
            'logger.apacheWire.level = info'
        )

    def test_path(self):
        config = ZAPLogConfiguration(self.external_config)
        self.assertEqual(config.get_path(), '/app/zap/log4j2.properties')

    def test_writes_base_properties(self):
        config = ZAPLogConfiguration(self.external_config)
        config.write_log_properties(self.stringio)
        expected = self.base_properties + '\n' + self.immutable_properties
        self.assertEqual(self.stringio.getvalue(), expected)

    def test_appends_extra_properties(self):
        config = ZAPLogConfiguration(ToConfig(zap_log_configuration=self.extra_properties))
        config.write_log_properties(self.stringio)
        expectation = self.base_properties + '\n' + self.extra_properties.replace(';', '\n')
        expectation = expectation + '\n' + self.immutable_properties
        self.assertEqual(self.stringio.getvalue(), expectation)
