# "Null" nomenclature is taken from "Null Object Pattern"
# https://en.wikipedia.org/wiki/Null_object_pattern

class NullZAPServer:

    def __init__(self, methods=[]):
        self.methods = methods

    def __getattr__(self, attr):
        defaultValue = self.methods[attr] if attr in self.methods else None
        return lambda *args, **kwargs: defaultValue

    def start(self):
        return NullZAPServerDaemon()


class NullActiveScan:

    def __init__(self, methods=[]):
        self.methods = methods

    def __getattr__(self, attr):
        defaultValue = self.methods[attr] if attr in self.methods else None
        return lambda *args, **kwargs: defaultValue


class NullZAProxy:

    def __init__(self, methods=[]):
        self.methods = methods

    def __getattr__(self, attr):
        defaultValue = self.methods[attr] if attr in self.methods else None
        return lambda *args, **kwargs: defaultValue

    def alerts(self, target):
        return []

    def zap_version(self):
        return '2.12.0'


class NullZAPServerDaemon:

    def __init__(self, methods=[]):
        self.methods = methods

    def __getattr__(self, attr):
        defaultValue = self.methods[attr] if attr in self.methods else None
        return lambda *args, **kwargs: defaultValue

    def shutdown(self):
        pass
