#!/usr/bin/env bash

# Sends Slack notification MSG to CI_SLACK_WEBHOOK_URL (which needs to be set).
# Message is sent to a designated slack room, with an emoji based on the webhook setup
# https://api.slack.com/apps/A022EJX8Q3T/incoming-webhooks

MSG=$1

if [ -z "$MSG" ]; then
    echo "Aborting, missing argument(s) - Use: $0 message"
    exit 1
fi

if [ -z "$CI_SLACK_WEBHOOK_URL" ]; then
    echo "Aborting, CI_SLACK_WEBHOOK_URL environment variable must be set"
    exit 1
fi

curl -X POST --data-urlencode 'payload={"text": "'"$MSG"'"}' "$CI_SLACK_WEBHOOK_URL"
