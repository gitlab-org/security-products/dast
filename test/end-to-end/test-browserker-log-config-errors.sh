#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

test_browserker_log_config_errors() {
  ./docker_run_dast --rm \
    -v "${PWD}":/output \
    --env DAST_BROWSER_SCAN="true" \
    --env DAST_BROWSER_NAVIGATION_TIMEOUT='60s"' \
    "${BUILT_IMAGE}" \
    /analyze -t http://httpstat.us/200 >output/test_browserker_log_config_errors.log 2>&1
  assert_equals "1" "$?" "Expected to exit with an error for invalid configuration"

  grep -q "BrowserkerError: Failure while running Browserker" output/test_browserker_log_config_errors.log &&
  grep -q "Browserker: FTL MAIN  failed to initialize browserker: failed to create configuration" output/test_browserker_log_config_errors.log &&
  assert_equals "0" "$?" "Browserker log output different than expected"
}
