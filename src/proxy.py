import logging
import socket
import time

from .models.errors import ProxyFailedToStartError


class Proxy:
    """Methods related to proxy service
    """

    def wait_for_proxy(
            self,
            host: str = '127.0.0.1',
            port: int = 3128,
            timeout: float = 10,
            wait_between_checks: float = 1) -> None:
        """Wait for proxy port to become available.

        This method waits for the proxies TCP port to become available by connecting to it.

        Args:
            host (str, optional): Proxy host. Defaults to '127.0.0.1'.
            port (int, optional): Proxy port. Defaults to 3128.
            timeout (float, optional): Maximum time to wait for the proxy port in seconds. Defaults to 10 seconds.
            wait_between_checks (float, optional): How long to wait between checks in seconds. Defaults to 1.

        Raises:
            ProxyFailedToStartError: Raised if the proxy fails to start in allotted time window
        """
        logging.info('Waiting for proxy service to become available...')
        start_time = time.perf_counter()
        def remaining_time(): return timeout - (time.perf_counter() - start_time)
        while True:
            try:
                with socket.create_connection((host, port), timeout=remaining_time()):
                    break
            except OSError as ex:
                logging.debug(f'Proxy service not yet available, retrying for {remaining_time():.2f} seconds.')
                time.sleep(wait_between_checks)
                if remaining_time() <= 0:
                    raise ProxyFailedToStartError(
                        'Proxy service failed to start, please contact support for assistance.') from ex
