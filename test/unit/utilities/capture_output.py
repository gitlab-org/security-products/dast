from collections import namedtuple
from contextlib import contextmanager
from io import StringIO


@contextmanager
def capture_output():
    import sys
    stdout = sys.stdout
    stderr = sys.stderr
    Output = namedtuple('Output', ['stdout', 'stderr'])

    try:
        tmp_stdout = sys.stdout = StringIO()
        tmp_stderr = sys.stderr = StringIO()

        yield Output(tmp_stdout, tmp_stderr)
    finally:
        sys.stdout = stdout
        sys.stderr = stderr
