import logging
from typing import Optional

from requests import Response
from requests.exceptions import ConnectionError, \
    HTTPError, \
    ProxyError, \
    ReadTimeout, \
    RequestException
from requests.packages import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from requests_pkcs12 import get

from src.configuration import Configuration
from .target_availability_check import TargetAvailabilityCheck


class TargetProbe:
    CHECK_ERROR_MESSAGES = {
        ConnectionError: 'failed to connect to target',
        ProxyError: 'failed to connect to ZAP',
        ReadTimeout: 'request timed out while waiting for data from server',
    }

    REQUEST_TIMEOUT = 5

    def __init__(
            self, target: str, config: Configuration,
            proxy: Optional[str] = None,
            follow_redirects: bool = True,
    ):
        self._config = config
        self._target = target
        self._proxy = proxy
        self._follow_redirects = follow_redirects

    def send_ping(self) -> TargetAvailabilityCheck:
        # thank you, we know that certificates should ideally be validated
        urllib3.disable_warnings(InsecureRequestWarning)

        try:
            logging.info(f'Requesting access to {self._target}...')

            request_args = {
                'timeout': self.REQUEST_TIMEOUT,
                'verify': False,
                'allow_redirects': self._follow_redirects,
                'headers': self._get_request_headers(),
            }

            if self._config.pkcs12_cert_filename and self._config.pkcs12_password is not None:
                request_args.update({
                    'pkcs12_filename': self._config.pkcs12_cert_filename,
                    'pkcs12_password': self._config.pkcs12_password,
                })

            if self._proxy:
                request_args.update({'proxies': {'http': self._proxy, 'https': self._proxy}})

            response = get(self._target, **request_args)
            return self._check_status(response)
        except (ConnectionError, ProxyError, ReadTimeout, RequestException) as error:
            error_message = self.CHECK_ERROR_MESSAGES.get(type(error), 'request failed')

            logging.info(f'{type(error).__name__}: {error_message}')

            return TargetAvailabilityCheck(False, self._config, unavailable_reason=error)

    def _get_request_headers(self):
        request_headers = {}

        if self._config.advertise_scan:
            request_headers.update({'Via': 'GitLab DAST'})

        if self._config.request_headers is not None:
            request_headers.update(self._config.request_headers)

        return request_headers

    def _check_status(self, response: Response) -> TargetAvailabilityCheck:
        try:
            response.raise_for_status()
        except HTTPError as error:
            logging.info(f'{type(error).__name__}: {response.status_code}')
            logging.debug(response.text)

            # 401 status indicates the host is available but may be behind basic auth
            if response.status_code == 401:
                return TargetAvailabilityCheck(True, self._config, response=response)

            return TargetAvailabilityCheck(
                False, self._config, response=response, unavailable_reason=error,
            )
        else:
            return TargetAvailabilityCheck(True, self._config, response=response)
