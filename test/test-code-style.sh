#!/bin/sh

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/.."

"$PROJECT_DIR"/test/code-style/lint-python.sh
"$PROJECT_DIR"/test/code-style/lint-shell-scripts.sh
"$PROJECT_DIR"/test/code-style/lint-changelog.sh
